<?php

class Ape extends Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "No";

    public function __construct($string)
    {
        $this->name = $string;
    }

    public function yell()
    {
        echo "Auooo";
    }
}
