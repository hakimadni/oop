<?php
require('animal.php');
require('frog.php');
require('ape.php');


$sheep = new Animal("shaun");

echo "Animal Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs Count : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"

$kodok = new frog("buduk");
echo "<br>";
echo "Animal Name : " . $kodok->name . "<br>";
echo "Legs Count : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : ";
$kodok->jump();

$sungokong  = new ape("kera sakti");

echo "<br><br>";
echo "Animal Name : " . $sungokong->name . "<br>";
echo "Legs Count : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Jump : ";
$sungokong->yell();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())